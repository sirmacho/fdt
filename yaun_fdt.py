# Implementação de algorítimo de arvore de decisão com base no artigo 
# Induction of fuzzy decision trees Yufei Yuana'*, Michael J. Shawb.

import math
import numpy as np
import pandas as pd 

#vaguenesse representa a imprecissão do conjunto e varia de 0 a 0.7 se usar log na base 2 varia de 0 a 1
# Equação 1 página 4.
def vagueness(df):
	m = len(df.index)
	ln = math.log
	E = np.zeros(len(df.columns))
	i = 0
	for coluna in df:
		for u in np.nditer(df[coluna]) :
			if(u != 0 and u != 1):
				E[i] = E[i] -1/m*(u*ln(u,2)+(1-u)*ln(1-u,2))
		i = i+1
	return E

# Calcula a ambiguidade de cada conjunto fuzzy da tabela sem relação com a classificação
# Equação 2 página 5
def ambiguity_dataframe(df):
    
	ln = math.log
	j = 0
	
	resultados = np.zeros(len(df.columns.levels[0]))
	
	for level in df.columns.levels[0]:
		df2 = df.loc[:,level]
	
		linhaNormalizada = df2.apply(lambda x: x/x.max(), axis=1)
		linhaNormalizadaOrdenada = linhaNormalizada.apply(np.sort, axis = 1) 
		derivada = linhaNormalizadaOrdenada.diff(periods=1, axis=1).fillna(linhaNormalizadaOrdenada.ix[:, [0]])
		derivada = derivada.ix[::,derivada.columns[::-1]]
		x = np.zeros(len(derivada.columns)) 
		i = 1
		for colunas in derivada:
			x[i-1] = ln(i) 
			i = i+1
			
		resultados[j] = derivada.multiply(x, axis=1).sum(axis=1).mean(axis=0)
		j = j+1

	return resultados

# Calcula a ambiguidade de uma evidencia
# Definição 10 página 8
def ambiguity_evidence(df):
	ln = math.log
	j = 0
	derivada = np.zeros(len(df))
	ordenado = np.sort(df)[::-1]
	
	for i in range(len(ordenado)-1):
		derivada[i] = ordenado[i] - ordenado[i+1]

	derivada[len(df)-1] = ordenado[len(df)-1]

	x = np.zeros(len(derivada));
	for i in range(len(derivada)):
		x[i] = ln(i+1)

	return np.multiply(x,derivada).sum()

# Calcula a ambiguidade de uma partição, equivalente a probabilidade condicional.
# Equação 10 página 8
def ambiguity_partition(dfp, dff,dfc):
	ln = math.log
	i = 0
	w = np.zeros(len(dfp.columns))
	G = np.zeros(len(dfp.columns))
	for E in dfp:
		w[i] = np.fmin(dfp.loc[:,E], dff).sum()
		G[i] = ambiguity_evidence(evidence(dfc,np.fmin(dfp.loc[:,E],dff)))
		i = i+1
	w = w/w.sum()
	S = np.multiply(w,G).sum()
	
	return S

# Calcula a ambiguidade de um atributo, caso especial da equação 10 onde a patição é o conjunto universo
def ambiguity_attribute(dfc, dfa, a):

	G = np.zeros(len(dfa.columns.levels[0]))
	j = 0
	for Set in dfa.columns.levels[0]:
		E = np.zeros(len(dfa.loc[:,Set].columns))
		w = np.zeros(len(dfa.loc[:,Set].columns))
		i = 0
		for Subset in dfa.loc[:,Set]:
			df = treshood(dfa.loc[:,Set].loc[:,Subset], a)
			E[i] = ambiguity_evidence(evidence(dfc, df))
			w[i] = df.sum()
			i = i+1
		w = w/w.sum()
		G[j] = (np.multiply(E,w).sum())
		j = j + 1
	return G

# Calcula o quanto um conjunto é um subconjunto do outro
# Equação 6 página 6
def subsethood(dfa, dfb):
	df = np.zeros(dfa.shape)
	df = np.fmin(dfa,dfb)
	S = df.sum() / dfa.sum()
	return S

# Calcula a possibilidade de um objeto ser classificado em uma determinada classe
# Equação 9 página 7 
def evidence(dfc, dfe):
	df = np.zeros(len(dfc.columns))
	i =0
	for level in dfc:
		df[i] =subsethood(dfe, dfc.loc[:,level])
		i = i+1
	df = df/df.max()
	return df

# Atribui um limiar de pertinencia a um conjunto fuzzy
def treshood(df,alpha):
	# df = df.where(df > alpha, 0)
	df = np.where(df < alpha,0,df)
	#print(df)
	return df

# Teste com dados iguais aos do documento do Yuan
dataFrame = pd.read_csv("data.csv",header=[0,1])
dataFrameSorted = dataFrame.sort_index(axis=1)


sunny = dataFrame.loc[:,'Outlook'].loc[:,'Sunny']
rain = dataFrame.loc[:,'Outlook'].loc[:,'Rain']
cloudy = dataFrame.loc[:,'Outlook'].loc[:,'Cloudy']
hot = dataFrame.loc[:,'Temperature'].loc[:,'Hot']
mild = dataFrame.loc[:,'Temperature'].loc[:,'Mild']
cool = dataFrame.loc[:,'Temperature'].loc[:,'Cool']
humid = dataFrame.loc[:,'Humidity'].loc[:,'Humid']
normal = dataFrame.loc[:,'Humidity'].loc[:,'Normal']
windy = dataFrame.loc[:,'Wind'].loc[:,'Windy']
not_windy = dataFrame.loc[:,'Wind'].loc[:,'Not_windy']
swimming = dataFrame.loc[:,'Plan'].loc[:,'Swimming']
volleyball = dataFrame.loc[:,'Plan'].loc[:,'Volleyball']
w_lifting = dataFrame.loc[:,'Plan'].loc[:,'W_lifting']

outlook = dataFrame.loc[:,'Outlook']
temperature = dataFrame.loc[:,'Temperature']
humidity = dataFrame.loc[:,'Humidity']
wind = dataFrame.loc[:,'Wind']
plan = dataFrame.loc[:,'Plan']


a = 0.5

attributy = dataFrame.drop('Plan', axis =1, level = 0)

print(ambiguity_attribute(plan,attributy, a))